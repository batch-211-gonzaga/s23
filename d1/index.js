// Object
/* - datatype used to represent real world objects
   - collection of related data/functionalities
   - data stored in objects are represented in a k:v pair

  key: property of an object
  value: actual data

  different data types may be stored
*/

// Two ways to create objects in js
// 1. object literal notation
//    let object = {}
// 2. object constructor notation
//    let object = new Class()

// Object literal notation
// camel case

let cellphone = {
  name: 'Nokia 3210',
  manufactureDate: 1999,
};

console.log('Result from creating objects using literal notation: ');
console.log(cellphone);

let cellphone2 = {
  name: 'iPhone 14',
  manufactureDate: 2022,
};

console.log('Result from creating objects using literal notation: ');
console.log(cellphone2);

let ninja = {
  name: 'Naruto Uzumaki',
  village: 'Konoha',
  children: ['Boruto', 'Himawari'],
};

console.log('Result from creating objects using literal notation: ');
console.log(ninja);

// Object constructor notation
// Uses a reusable function to create several objects that have the same data structure

// "this" refers to object's properties

function Laptop(name, manufactureDate) {
  this.name = name;
  this.manufactureDate = manufactureDate;
}

let laptop = new Laptop('Lenovo', 2008);

console.log(laptop);

let oldLaptop = Laptop('Portal R2E CCMC', 1980); // undefined because no 'new' Laptop() has no return

// Create empty objects
let computer = {};
let myComputer = new Object();

console.log(computer);
console.log(myComputer);

// Accessing object properties

// using dot notation
console.log('Result of dot notation: ' + laptop.name);

//Using square bracket notation
console.log('Result of square bracket notation: ' + laptop['name']);

let arrayObj = [cellphone, cellphone2];

console.log(arrayObj[0]['name']);
console.log(arrayObj[0].name);


// Initializing/Adding/Deleting/Reasigning Object Properties
let car = {};
console.log('Current value of car object: ');
console.log(car);


car.name = 'Honda Civic';
console.log('Adding prop using dot notation');
console.log(car);

car['manufacture date'] = 2019;
console.log('Result of adding prop using bracket notation');
console.log(car);

// Delete object properties
delete car['manufacture date'];
console.log('After delete car[\'manufacture date\'];');

car['manufactureDate'] = 2019;
console.log(car);

// Reassigning object values
car.name = 'Toyota Vios';
console.log('After reassigning');
console.log(car);

// Object method
let person = {
  name: 'John',
  talk: function () {
    console.log('Hello, my name is ' + this.name);
  }
}

console.log(person);
console.log(person.talk());

person.walk = function (steps) {
  console.log(this.name + ' walked ' + steps + ' steps forward.');
}
person.walk(200)

let friend = {
  firstName: 'Joe',
  lastName: 'Smith',
  address: {
    city: 'Austin',
    country: 'Texas',
  },
  emails: ['joe@mail.com', 'joesmith@mail.xyz'],
  introduce: function () {
    console.log('Hello, my name is ' + this.firstName + ' ' + this.lastName +
      '. I live in ' + this.address.city + ', ' + this.address.country
    );
  }
}

friend.introduce();

/* 
  Scenario:
  1. game with several pokemon interacting with each other
  2. same stats, props, functions

  Stats:
  name:
  level:
  health: level * 2
  attack: level
  */

// create an object constructor

function Pokemon(name, level) {
  this.name = name;
  this.level = level;
  this.health = level * 2;
  this.attack = level;
  this.tackle = function (target) {
    console.log(this.name + ' tackled ' + target.name);
    target.health -= this.attack;
    console.log(target.name + "'s health is now " + target.health);
    if (target.health <= 0) {
      target.faint();
    }
  },
  this.faint = function () {
    console.log(this.name + ' fainted');
  }
  // Mini activity
  // if the target health is less than or equal to 0, invoke faint(). Otherwise, print the pkmn's new health.
  // reduce the target's object health property

};

let pikachu = new Pokemon('Pikachu' , 88);
console.log(pikachu);

let rattata = new Pokemon('Ratata', 10);
console.log(rattata);

